1. Add gem 'bootstrap-sass', '~> 3.1.1' to Gemfile and bundle
2. Remove :
    *= require_tree .
    *= require_self
    
    from application.css
3. Rename application.css to application.css.scss
4. Add //= require_tree bootstrap to application.js



------------------------------------------------------------
Turn off Coffeescript Rails 4.1
------------------------------------------------------------

1. Remove sass-rails from Gemfile
2. Add 

    config.generators do |g|
      g.javascript_engine :js
    end

    to application.rb

------------------------------------------------------------
Twitter Bootstrap 3.1 with Rails 4.1
------------------------------------------------------------

1. Add gem 'bootstrap-sass' to Gemfile and bundle.

    This gem adds Bootstrap CSS and javascript files to Rails asset pipeline.
    The bootstrap-sass gem is the official port of Bootstrap for Sass.

2. Rename application.css to application.css.scss

    This will allow you to use Sass syntax in your stylesheet.
    You can add any CSS rules to the file for use in your views. This is a 
    global CSS file.
    
    This file serves as a manifest, providing a list of files that should be
    concatenated and included in the single CSS file that is delivered to the 
    browser. 
    
3. Create a file app/assets/stylesheets/framework_and_overrides.css.scss

    This file is for style rules provided by Bootstrap.

4. Modify the file app/assets/javascripts/application.js. Add
    //= require bootstrap
    to application.js before the line:
    //= require_tree .

5. Change layout to include partials in application.html.erb
    
